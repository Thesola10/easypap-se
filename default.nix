{ pkgs, stdenv, fxt, ... }:

stdenv.mkDerivation {
  name = "easypap";

  src = ./.;

  nativeBuildInputs = with pkgs;
    [ pkg-config
      flex
    ];

  buildInputs = with pkgs;
    [ fxt
      SDL2
      SDL2_ttf
      SDL2_image
      hwloc
      openssl
      openmpi
      opencl-headers
      ocl-icd
    ];

  installPhase = ''
    mkdir -p $out/bin
    cp bin/* $out/bin
  '';
}
