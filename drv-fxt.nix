{ pkgs ? import <nixpkgs> {} }:

pkgs.stdenv.mkDerivation rec {
  pname = "fxt";
  version = "0.3.14";
  src = builtins.fetchTarball {
    url = "https://download.savannah.nongnu.org/releases/fkt/${pname}-${version}.tar.gz";
    sha256 = "094ani1p9kc57smzy9fwb9b8pjz76wzzhqxlm74aygg5jqadzbv9";
  };

  nativeBuildInputs = with pkgs; [
    perl
    pkg-config
  ];

  preBuild = ''
    patchShebangs *.pl
  '';
}
