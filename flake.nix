{ description = "easy-to-use programming environment to learn parallel programming";

  inputs."nixpkgs".url = github:NixOS/nixpkgs;
  inputs."nixgl".url = github:nix-community/nixGL;
  inputs."nixgl".inputs."nixpkgs".follows = "nixpkgs";

  outputs = { self, nixpkgs, nixgl, flake-utils, ... }:
  flake-utils.lib.eachDefaultSystem
    (system:
    let pkgs = import nixpkgs { inherit system; };
        fxt = pkgs.callPackage ./drv-fxt.nix {};
    in
    { packages.default = pkgs.callPackage ./default.nix { inherit fxt; };
      devShells = 
      let
        mkOpenCL = icd: pkgs.mkShell {
          OCL_ICD_VENDORS = "${icd}/etc/OpenCL/vendors/";

          buildInputs = [ nixgl.packages.${system}.nixGLIntel ];
          inputsFrom = [ self.devShells.${system}.default ];

          venvDir = ".venv";
        };
      in
      {
        opencl-mesa = mkOpenCL pkgs.mesa.opencl;
        opencl-intel = mkOpenCL pkgs.intel-compute-runtime;
        opencl-rocm = mkOpenCL pkgs.rocm-opencl-icd;
        default = pkgs.mkShell {
          inputsFrom = [ self.packages.${system}.default ];
          buildInputs = with pkgs.python3Packages; with pkgs; [
            venvShellHook
            pandas
            seaborn
          ];

          "NIX_ENFORCE_NO_NATIVE" = "0";

          venvDir = ".venv";
        };
      };
    });
}
